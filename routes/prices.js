var express = require('express');
var router = express.Router();
var Price = require('../models/Price');

/**
* Index Route
*/
router.get('/', function (req, res, next) {
  var limit = req.query.limit || 25;
  var skip = req.query.skip || 0;
  var productId = req.query.product;

  var query = productId ? {product: productId} : {};

  Price
  .find(query)
  .lean()
  .limit(limit)
  .skip(skip)
  .exec(function(err, prices) {
    if (handleErr(err)) return;

    if(prices) {
      res.json(prices);
    } else {
      res.json([]);
    }
  });

});

/**
* Show Route
*/
router.get('/:id', function (req, res, next) {

  Price
  .findById(req.params.id)
  .exec(function(err, price) {
    if (handleErr(err)) return;

    if(price) {
      res.json(price);
    } else {
      res.json(null);
    }
  });
});

function handleErr(err) {
  if (err) {
    console.log(err.message);
    res.status(500);
    res.json(err.message);
    return true;
  } else {
    return false;
  }
}

module.exports = router;
