var express = require('express');
var router = express.Router();
var Product = require('../models/Product');

/**
 * Index Route
 */
router.get('/', function (req, res, next) {
  var limit = req.query.limit || 25;
  var skip = req.query.skip || 0;
  var productName = req.query.name;

  var query = productName ? {'name' : new RegExp(productName, 'i')} : {};

  Product
    .find(query)
    .lean()
    .limit(limit)
    .skip(skip)
    .sort({created: 'desc'})
    .exec(function(err, products) {
      if (handleErr(err)) return;

      if(products) {
        res.json(products);
      } else {
        res.json([]);
      }
    });

});

/**
 * Show Route
 */
router.get('/:id', function (req, res, next) {

  Product
    .findById(req.params.id)
    .exec(function(err, product) {
      if (handleErr(err)) return;

      if(product) {
        res.json(product);
      } else {
        res.json(null);
      }
    });
});

function handleErr(err) {
  if (err) {
    console.log(err.message);
    res.status(500);
    res.json(err.message);
    return true;
  } else {
    return false;
  }
}

module.exports = router;
