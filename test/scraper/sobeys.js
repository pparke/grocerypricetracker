var expect = require('chai').expect;
var should = require('chai').should();

var sobeys = require('../../lib/scraper/sobeys');
var store = require('../../lib/scraper/store');

describe('Sobeys scraper test', function() {

  this.timeout(15000);

  var data;
  var date = '2015-01-24 00:00:00';

  it('should execute the scraper', function(done) {
    //sobeys({filepath: __dirname+'/../../test/data/thu-september-11-17/'})
    sobeys()
    .then(function(res){
      expect(res.date);
      expect(res.lastPage);
      data = res;
      console.log(res.cards.length);
      done();
    })
    .fail(function(reason) {
      console.log("Failed:\n"+reason);
      throw new Error(reason);
    });
  });

  it('should store the data in the db', function(done) {
    store({cards: data.cards})
    .then(function(count) {
      expect(count);
      expect(count.products);
      expect(count.prices);
      console.log(count);
      done();
    })
    .fail(function (err) {
      console.log(err);
      done();
    });
  });
});
