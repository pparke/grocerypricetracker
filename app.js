var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var db = require('./lib/mongodb/db');
var handlebars = require('express-handlebars');
var hbsHelpers = require('./lib/handlebars/helpers');


//==============================================================================
// Require Routes
//==============================================================================
var index = require('./routes/index');
var about = require('./routes/about');
var products = require('./routes/products');
var prices = require('./routes/prices');

//==============================================================================
// App
//==============================================================================
var app = express();

//==============================================================================
// View Engine Setup
//==============================================================================
var hbs = handlebars.create({
  extname: '.hbs',
  defaultLayout: 'layout',
  helpers: hbsHelpers
});

app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', hbs.engine);
app.set('view engine', '.hbs');


//==============================================================================
// Other
//==============================================================================
// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//==============================================================================
// Add Headers
//==============================================================================
app.use(function (req, res, next) {

  // Allow Origin
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost');

  // Allow Methods
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');

  // Allow Headers
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  next();
});

//==============================================================================
// Routes
//==============================================================================
app.use('/', index);
app.use('/about', about);
app.use('/api/v1/products', products);
app.use('/api/v1/prices', prices);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

//==============================================================================
// Error Handlers
//==============================================================================

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

//==============================================================================
// Export
//==============================================================================
module.exports = app;
