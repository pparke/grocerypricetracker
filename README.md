# Grocery Price Tracker#
## CIS 2250 ##

Grocery Price Tracker is an application that acts as a front end for the grocery price web scraper
which pulls the current sale information from a grocery store flyer and saves it in a database.
Each uniquely named product is recorded once and all of its historical sale prices are recorded
along with the date they were retrieved.  This allows the application to display a chart of the recent
sale prices for an item.  The application provides a basic search by name and an option to browse
products.