var db = require('../lib/mongodb/db');
var Schema = db.Schema;
var ObjectId = Schema.Types.ObjectId;
var findOrCreate = require('mongoose-findorcreate');

var ProductSchema = new db.Schema({
  name    : String,
  image   : String,
  created : {type: Date, default: Date.now}
});

ProductSchema.plugin(findOrCreate);

var Product = db.mongoose.model('Product', ProductSchema);

module.exports = Product;
