var db = require('../lib/mongodb/db');
var Schema = db.Schema;
var ObjectId = Schema.Types.ObjectId;

var PriceSchema = new db.Schema({
  product  : {type: ObjectId, ref: 'Product'},
  value    : Number,
  unit     : String,
  created  : {type: Date, default: Date.now}
});

var Price = db.mongoose.model('Price', PriceSchema);

module.exports = Price;
