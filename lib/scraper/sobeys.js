var request = require('superagent');
var cheerio = require('cheerio');
var fs = require('fs');
var Q = require('q');
var merge = require('merge');
var async = require('async');

function sobeys(options) {


  var html = '';
  var query = 'page';
  var pageNum = 1;
  var json = {};
  var loader;
  var path;

  var deferred = Q.defer();

  (function(){
    if(options && options.filepath) {
      path = options.filepath;
      loader = loadFromFile;

    } else {
      path = 'https://www.sobeys.com/en/flyer';
      loader = loadFromWeb;
    }

    return loader(path, query, pageNum);
  })()
    .then(function(html){
      // prepare the dom
      var $ = cheerio.load(html);

      // get the date and number of pages
      var result = getDateAndPages($);
      json = merge(json, result);

      // get the data for the cards
      getCardData(loader, path, query, pageNum, json.lastPage)
      .then(function(cards) {
        json.cards = cards;

        deferred.resolve(json);
      }).done();
    });

  return deferred.promise;

}

function getCardData(fn, path, name, count, end) {
  var deferred = Q.defer();

  var cards = [];
  async.whilst(function(){ return count <= end; },
  function(next) {
    console.log(count);

      // load the html from either a file
      // or from the web
      fn(path, name, count)
      .then(function(html) {
        console.log('success')

        $ = cheerio.load(html);

        // get the array of cards
        result = getFlyerData($);
        // append to the final array of cards
        cards = cards.concat(result);

        count += 1;
        next();
      },
      // only log an error so it won't effect
      // the other cards collected
      function(err) {
          console.log('failed to load %s%s%s.html',path,name,count);
          count += 1;
          next();
      });
    // resolve or reject once whilst has ended
  }, function(err) {
      if (err) deferred.reject(err);
      else deferred.resolve(cards);
  });

  return deferred.promise;
}

function getFlyerData($){
  // get all the cards
  var cards = $('.card-top');
  var out = [];

  // collect the card data for each card
  cards.each(function(index, card) {
    cardData = {};
    cardData.name = $(card).children('.card-inset')
                            .children('h6')
                            .text();

    cardData.dollars = $(card).children('.card-inset')
                              .children('.price')
                              .children('.price-amount')
                              .contents()
                              .filter(function(){
                                return this.nodeType === 3;
                              })
                              .text();

    cardData.cents = $(card).children('.card-inset')
                            .children('.price')
                            .children('.price-amount')
                            .children('sup')
                            .contents()
                            .filter(function(){
                              return this.nodeType === 3;
                            })
                            .text();

    cardData.unit = $(card).children('.card-inset')
                            .children('.price')
                            .children('.price-amount')
                            .children('sup')
                            .children('.price-unit')
                            .text();

    cardData.image = $(card).children('img.card-image-print')
                            .attr('src');

    out.push(cardData);

  });

  return out;
}


function getDateAndPages($){

  var dateHeader = $('.h3-editorial');
  var date = dateHeader.text();
  var href = $('span.last').children('a').first().attr('href');
  var lastPage = parseInt(href.match(/[0-9]+$/)[0]);

  return {date: date, lastPage: lastPage};
}

function loadFromFile(filepath, name, num) {
  var deferred = Q.defer();

  fs.readFile(filepath+name+num+'.html', 'utf-8', function(err, data){
    if(err) {
      console.log(err);
      deferred.reject(new Error(err));
    }
    else deferred.resolve(data);
  });

  return deferred.promise;
}

function loadFromWeb(url, query, pageNum) {
  var deferred = Q.defer();

  request
  .get(url + '?' + query + '=' + pageNum)
  .end(function(res){
    deferred.resolve(res.text);
  });

  return deferred.promise;
}

module.exports = sobeys;
