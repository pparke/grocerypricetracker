var mongoose = require('mongoose');
var Product = require('../../models/Product');
var Price = require('../../models/Price');
var Q = require('q');
var async = require('async');


function store(options) {
  var deferred = Q.defer();

  var cards = options.cards;
  var date = options.date;

  var count = {
    products: 0,
    prices: 0
  };

  async.eachLimit(cards, 5, function(card, done) {

    var prodDef = {name: card.name, image: card.image};
    if(date) prodDef.created = date;

    createProduct(prodDef)
    .then(function(record) {

      // increment the count if a new product was created
      if(record.created) count.products += 1;

      var value = parsePrice(card.dollars, card.cents);

      // create the price using the product id
      var priceDef = {product: record.product._id,
                      value: value,
                      unit: card.unit};
      if (date) priceDef.created = date;

      createPrice(priceDef)
      .then(function(price) {
        count.prices += 1;

        done();
      });
    })
    .fail(function(err) {
      console.log(err);
      done();
    });
  },
  function(err) {
    if (err) {
      console.log(err);
      deferred.reject(new Error(err));
    } else {
      console.log('resolved')
      deferred.resolve(count);
    }
  });

  return deferred.promise;
}

function parsePrice(dollars, cents) {
  if (cents === 'off') throw new Error("Percent Price");
  var ndollars;
  var ncents;
  // parse any fraction in the dollar variable
  var fractions = dollars.split('/');
  ndollars = fractions.length > 1 ? fractions[0] / fractions[1] : fractions[0];
  ndollars = ndollars || 0;
  ndollars = parseFloat(ndollars);

  // make sure we have a value for cents
  // and that it is an integer
  ncents = cents || 0;
  ncents = parseInt(ncents);
  ncents *= 0.01;

  if(isNaN(ndollars)) {
    console.log("Could not parse " + dollars);
    ndollars = 0;
  }
  if(isNaN(ncents)) {
    console.log("Could not parse " + cents);
    ncents = 0;
  }

  return (ndollars + ncents);
}

function createProduct(def) {
  var deferred = Q.defer();

  Product.findOrCreate(def, function(err, product, created) {
    if (err) {
      console.log(err);
      deferred.reject(new Error(err));
    } else {

      // update the image
      if(!created) {
        product.update({image: def.image}, function(err) {
          if(err) {
            console.log(err);
            deferred.reject(new Error(err));
          } else {

            deferred.resolve({product: product, created: created});
          }
        });
      // return with new product
      } else {
        deferred.resolve({product: product, created: created});
      }


    }
  });

  return deferred.promise;
}

function createPrice(def) {
  var deferred = Q.defer();

  var record = new Price(def);

  record.save(function(err, price) {
    if (err) {
      console.log (err);
      deferred.reject(new Error(err));
    } else {
      deferred.resolve(price);
    }
  });

  return deferred.promise;
}

module.exports = store;
