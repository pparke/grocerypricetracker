// helper methods for handlebars
module.exports = {

  // test if two params are equal
  isEqual: function(a, b, options) {
    if(a == b) {
      return options.fn(this);
    } else {
      return options.inverse(this);
    }
  },

  // produce a range of numbers
  range: function(min, max, step, options) {
    if (!options){
      options = step;
      step = 1;
    }
    var ret = "";

    for(var i = min; i <= max; i += step) {
      ret = ret + options.fn(i);
    }

    return ret;
  },

  // for each value in a collection
  forIn: function(obj, keys, options) {
    if (!options){
      options = keys;
      keys = obj;
    }
    var ret = "";

    if(keys instanceof Array) {
      keys.forEach(function(key) {
        if(obj[key] === null) {
          ret = ret + options.inverse();
        } else {
          ret = ret + options.fn(obj[key]);
        }
      });
    } else {
      for(var key in keys) {
        if(obj.hasOwnProperty(key)){
          if(obj[key] === null) {
            ret = ret + options.inverse();
          } else {
            ret = ret + options.fn(obj[key]);
          }
        }
      }
    }

    return ret;
  }
};
