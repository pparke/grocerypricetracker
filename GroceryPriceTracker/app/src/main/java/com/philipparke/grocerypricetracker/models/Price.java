package com.philipparke.grocerypricetracker.models;

import java.util.Date;

/**
 * Price
 * Defines model for product price
 */
public class Price {
    private String _id;
    private String product;
    private float value;
    private String unit;
    private Date created;

    public Price() {

    }

    public String getId() {
        return _id;
    }

    public void setId(String id) {
        this._id = id;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }


}
