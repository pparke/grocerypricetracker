package com.philipparke.grocerypricetracker.REST;

import com.philipparke.grocerypricetracker.models.Price;
import com.philipparke.grocerypricetracker.models.Product;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * API Service
 * Defines interface for the API
 */
public interface ApiService {

    @GET("/api/v1/products")
    public void getProducts(@Query("name") String name, @Query("limit") int limit, @Query("skip") int skip, Callback<ArrayList<Product>> callback);

    @GET("/api/v1/products/{id}")
    public void getProduct(@Path("id") String id, Callback<Product> callback);

    @GET("/api/v1/prices")
    public void getPrices(@Query("product") String id, Callback<ArrayList<Price>> callback);

    @GET("/api/v1/prices/{id}")
    public void getPrice(@Path("id") String id, @Query("product") String product, Callback<Price> callback);
}
