package com.philipparke.grocerypricetracker.models;

import java.util.Date;

/**
 * Product
 * Defines a model for a product
 * also contains intent message keys for products
 */
public class Product {

    public final static String PRODUCT_ID = "com.philipparke.grocerypricetracker.PRODUCT_ID";
    public final static String PRODUCT_NAME = "com.philipparke.grocerypricetracker.PRODUCT_NAME";

    private String _id;
    private String name;
    private String image;
    private Date created;

    public Product() {
    }

    public String getId() {
        return _id;
    }

    public void setId(String id) {
        this._id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
