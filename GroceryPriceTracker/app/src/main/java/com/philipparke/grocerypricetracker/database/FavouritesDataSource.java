package com.philipparke.grocerypricetracker.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.philipparke.grocerypricetracker.models.Favourite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by philip on 26/02/15.
 */
public class FavouritesDataSource {

    // Database fields
    private SQLiteDatabase database;
    private FavouritesDbHelper dbHelper;
    private String[] allColumns = { FavouritesDbHelper.COLUMN_ID,
            FavouritesDbHelper.COLUMN_NAME, FavouritesDbHelper.COLUMN_PRODUCT_ID };

    public FavouritesDataSource(Context context) {
        dbHelper = new FavouritesDbHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Favourite createFavourite(String name, String product_id) {
        ContentValues values = new ContentValues();

        values.put(FavouritesDbHelper.COLUMN_NAME, name);
        values.put(FavouritesDbHelper.COLUMN_PRODUCT_ID, product_id);

        long insertId = database.insert(FavouritesDbHelper.TABLE_FAVOURITES, null,
                values);
        Cursor cursor = database.query(FavouritesDbHelper.TABLE_FAVOURITES,
                allColumns, FavouritesDbHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Favourite favourite = cursorToFavourite(cursor);
        cursor.close();
        return favourite;
    }

    public void deleteFavourite(String id) {
        System.out.println("Favourite deleted with id: " + id);
        String whereClause = FavouritesDbHelper.COLUMN_PRODUCT_ID + "=?";
        String[] selectionArgs = {id};
        database.delete(FavouritesDbHelper.TABLE_FAVOURITES,
                 whereClause, selectionArgs);
    }

    public List<Favourite> getAllFavourites() {
        List<Favourite> favourites = new ArrayList<Favourite>();

        Cursor cursor = database.query(FavouritesDbHelper.TABLE_FAVOURITES,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Favourite favourite = cursorToFavourite(cursor);
            favourites.add(favourite);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return favourites;
    }

    public boolean containsId(String id) {
        String[] tableColumns = new String[]{"product_id"};
        String whereClause = "product_id = ?";
        String[] selectionArgs = {id};
        Cursor cursor = database.query(FavouritesDbHelper.TABLE_FAVOURITES,
                tableColumns, whereClause, selectionArgs, null, null, null, null);

        boolean exists = true;

        if (cursor.getCount() <= 0) exists = false;

        cursor.close();
        return exists;
    }

    private Favourite cursorToFavourite(Cursor cursor) {
        Favourite favourite = new Favourite();
        favourite.setId(cursor.getLong(0));
        favourite.setName(cursor.getString(1));
        favourite.setProductId(cursor.getString(2));
        return favourite;
    }
}
