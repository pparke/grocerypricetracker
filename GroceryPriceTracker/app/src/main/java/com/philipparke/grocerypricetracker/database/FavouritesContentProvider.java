package com.philipparke.grocerypricetracker.database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Adapted from http://www.vogella.com/tutorials/AndroidSQLite/article.html
 */
public class FavouritesContentProvider extends ContentProvider {

        private FavouritesDbHelper database;

        private static final int FAVOURITES = 10;
        private static final int FAVOURITE_ID = 20;

        private static final String AUTHORITY = "com.philipparke.grocerypricetracker.contentprovider";

        private static final String BASE_PATH = "grocerypricetracker";
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/favourites";
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/favourite";

        private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        static {
            sURIMatcher.addURI(AUTHORITY, BASE_PATH, FAVOURITES);
            sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", FAVOURITE_ID);
        }

        @Override
        public boolean onCreate() {
            database = new FavouritesDbHelper(getContext());
            return false;
        }

        @Override
        public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

            SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

            checkColumns(projection);

            queryBuilder.setTables(FavouritesDbHelper.TABLE_FAVOURITES);

            int uriType = sURIMatcher.match(uri);
            switch (uriType) {
                case FAVOURITES:
                    break;
                case FAVOURITE_ID:
                    queryBuilder.appendWhere(FavouritesDbHelper.COLUMN_ID + "=" + uri.getLastPathSegment());
                    break;
                default:
                    throw new IllegalArgumentException("Bad URI: " + uri);
            }

            SQLiteDatabase db = database.getWritableDatabase();
            Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
            cursor.setNotificationUri(getContext().getContentResolver(), uri);

            return cursor;
        }

        @Override
        public String getType(Uri uri) {
            return null;
        }

        @Override
        public Uri insert(Uri uri, ContentValues values) {
            throw new UnsupportedOperationException();
        }

        @Override
        public int delete(Uri uri, String selection, String[] selectionArgs) {
            throw new UnsupportedOperationException();
        }

        @Override
        public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
            throw new UnsupportedOperationException();
        }

        private void checkColumns(String[] projection) {
            String[] available = { FavouritesDbHelper.COLUMN_ID , FavouritesDbHelper.COLUMN_NAME, FavouritesDbHelper.COLUMN_PRODUCT_ID };
            if (projection != null) {
                HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
                HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
                if (!availableColumns.containsAll(requestedColumns)) {
                    throw new IllegalArgumentException("Columns don't exist");
                }
            }
        }

    }
