package com.philipparke.grocerypricetracker.REST;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * REST Client
 * Singleton object that communicates with the REST API
 */
public class RestClient {

    private static final String BASE_URL = "http://ec2-54-152-87-41.compute-1.amazonaws.com:8080";
    private static ApiService apiService;

    static {
        setupRestClient();
    }

    private static void setupRestClient(){
        Gson gson = new GsonBuilder()
                        .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                        .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                                        .setLogLevel(RestAdapter.LogLevel.FULL)
                                        .setEndpoint(BASE_URL)
                                        .setConverter(new GsonConverter(gson))
                                        .build();


        apiService = restAdapter.create(ApiService.class);
    }

    public static ApiService getApiService(){
        return apiService;
    }
}
