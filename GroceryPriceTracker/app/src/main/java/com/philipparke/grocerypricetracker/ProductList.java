package com.philipparke.grocerypricetracker;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.philipparke.grocerypricetracker.REST.RestClient;
import com.philipparke.grocerypricetracker.database.FavouritesDataSource;
import com.philipparke.grocerypricetracker.models.Product;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Sets up the product list activity and loads products from the API
 */
public class ProductList extends ListActivity {


    // The list of retrieved products
    private List<Product> productList;
    private ProductListAdapter adapter;
    private String searchTerm;
    private int limit = 10;
    private int skip = 0;
    private ListView listView;
    FavouritesDataSource datasource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        searchTerm = intent.getStringExtra(MainActivity.SEARCH_QUERY);

        // Get the datasource to save to
        datasource = new FavouritesDataSource(this);
        datasource.open();

        Toast.makeText(this, searchTerm + " search", Toast.LENGTH_LONG).show();

        //==========================================================================================
        // View setup
        //==========================================================================================
        setContentView(R.layout.activity_product_list);

        listView = (ListView) findViewById(android.R.id.list);

        // LoadMore button
        Button btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");

        // Adding Load More button to lisview at bottom
        listView.addFooterView(btnLoadMore);

        btnLoadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // Starting a new async task
                new LoadProducts().execute();
            }
        });

        //==========================================================================================
        // Adapter setup
        //==========================================================================================
        adapter = new ProductListAdapter(this, new ArrayList<Product>(), datasource);

        setListAdapter(adapter);

        new LoadProducts().execute();

    }



    @Override
    protected void onListItemClick(ListView listView, View view, int position, long id) {
        // get the value from the item at postion in the list adapter
        Product item = (Product) getListAdapter().getItem(position);

        // open the chart view for the product
        Intent intent = new Intent(this, ProductChart.class);
        // send the id to lookup prices for
        intent.putExtra(Product.PRODUCT_ID, item.getId());
        intent.putExtra(Product.PRODUCT_NAME, item.getName());

        startActivity(intent);
    }

    /**
     * Load Products
     * Background task to load more products either on activity start
     * or when the load more button is clicked.
     */
    private class LoadProducts extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            runOnUiThread(new Runnable() {
                public void run() {
                    RestClient.getApiService().getProducts(searchTerm, limit, skip, new Callback<ArrayList<Product>>() {

                        @Override
                        public void success(ArrayList<Product> products, Response response) {

                            productList = products;

                            adapter.addAll(products);

                            skip += limit;

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            // something went wrong
                        }
                    });
                }
            });
            return (null);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    @Override
    protected void onResume() {
        datasource.open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }

}
