package com.philipparke.grocerypricetracker;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.philipparke.grocerypricetracker.REST.RestClient;
import com.philipparke.grocerypricetracker.models.Price;
import com.philipparke.grocerypricetracker.models.Product;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Product Chart
 * Loads prices and sets up the product graph fragment.
 */
public class ProductChart extends Activity {

    private String productName;
    private String productId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_chart);

        Intent intent = getIntent();

        productId = intent.getStringExtra(Product.PRODUCT_ID);
        productName = intent.getStringExtra(Product.PRODUCT_NAME);

        //==========================================================================================
        // Setup the Fragment
        //==========================================================================================
        // Begin the transaction
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        // Create the chart
        LineChartFragment productChart = new LineChartFragment();

        // Add the new fragment
        ft.add(R.id.chart_placeholder, productChart);

        // Execute the changes specified
        ft.commit();

        //==========================================================================================
        // Get the prices for the product
        //==========================================================================================
        //loadProducts(productId);
        new LoadPrices().execute();


    }

    /**
     * Load Prices
     * Background task to load the prices for a product
     */
    private class LoadPrices extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            runOnUiThread(new Runnable() {
                public void run() {
                    RestClient.getApiService().getPrices(productId, new Callback<ArrayList<Price>>() {

                        @Override
                        public void success(ArrayList<Price> prices, Response response) {

                            // Begin the transaction
                            FragmentTransaction ft = getFragmentManager().beginTransaction();

                            // Create the chart
                            LineChartFragment productChart = new LineChartFragment();

                            // Set up the chart
                            productChart.setPrices(prices);
                            productChart.setTitle(productName);

                            ft.replace(R.id.chart_placeholder, productChart);

                            ft.commit();


                        }

                        @Override
                        public void failure(RetrofitError error) {
                            // something went wrong
                        }
                    });
                }
            });
            return (null);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_product_chart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
