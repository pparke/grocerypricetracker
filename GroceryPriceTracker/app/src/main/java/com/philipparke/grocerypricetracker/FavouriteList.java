package com.philipparke.grocerypricetracker;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.philipparke.grocerypricetracker.database.FavouritesDataSource;
import com.philipparke.grocerypricetracker.models.Favourite;
import com.philipparke.grocerypricetracker.models.Product;

import java.util.List;

/**
 * Favourite List
 * Sets up the favourite list activity and loads favourites from the database.
 */
public class FavouriteList extends ListActivity {

    private FavouritesDataSource datasource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite_list);

        datasource = new FavouritesDataSource(this);
        datasource.open();

        List<Favourite> values = datasource.getAllFavourites();

        // use the SimpleCursorAdapter to show the
        // elements in a ListView
        ArrayAdapter<Favourite> adapter = new ArrayAdapter<Favourite>(this,
                android.R.layout.simple_list_item_1, values);
        setListAdapter(adapter);
    }

    @Override
    protected void onListItemClick(ListView listView, View view, int position, long id) {
        // get the value from the item at postion in the list adapter
        Favourite item = (Favourite) getListAdapter().getItem(position);

        // open the chart view for the product
        Intent intent = new Intent(this, ProductChart.class);
        // send the id to lookup prices for
        intent.putExtra(Product.PRODUCT_ID, item.getProductId());
        intent.putExtra(Product.PRODUCT_NAME, item.getName());

        startActivity(intent);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_favourite_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
