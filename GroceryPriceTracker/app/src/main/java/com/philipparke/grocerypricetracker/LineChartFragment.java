package com.philipparke.grocerypricetracker;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.content.res.Resources;
import android.widget.TextView;
import android.widget.Toast;

import com.echo.holographlibrary.Line;
import com.echo.holographlibrary.LineGraph;
import com.echo.holographlibrary.LineGraph.OnPointClickedListener;
import com.echo.holographlibrary.LinePoint;
import com.philipparke.grocerypricetracker.models.Price;

import org.w3c.dom.Text;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Line Chart Fragment
 * Draws the line chart using the prices set.
 */
public class LineChartFragment extends Fragment {

    private List<Price> prices;
    private String title;

    public void setPrices(List<Price> prices) {

        this.prices = prices;

        // sort by date
        Collections.sort(this.prices, new Comparator<Price>() {
            public int compare(Price p1, Price p2) {
                return p1.getCreated().compareTo(p2.getCreated());
            }
        });
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_linegraph, container, false);
        final Resources resources = getResources();
        TextView graphTitle = (TextView) v.findViewById(R.id.graphTitle);
        graphTitle.setText(title);

        Line l = new Line();
        l.setUsingDips(true);
        LinePoint p;
        double x = 0;
        float maxPrice = 0;
        if (prices != null && prices.size() > 0) {
            for (Price price : prices) {
                String value = String.valueOf(price.getValue());
                Toast.makeText(getActivity(), "$" + value, Toast.LENGTH_LONG).show();
                p = new LinePoint();
                float priceAmount = price.getValue();
                if (priceAmount > maxPrice) maxPrice = priceAmount;
                p.setY(priceAmount);
                p.setX(x);
                p.setColor(resources.getColor(R.color.red));
                p.setSelectedColor(resources.getColor(R.color.transparent_blue));
                l.addPoint(p);
                x += 1;
            }


            LineGraph li = (LineGraph) v.findViewById(R.id.linegraph);
            li.setUsingDips(true);
            li.addLine(l);
            if (maxPrice > 10) {
                li.setRangeY(0, maxPrice+10);
            } else {
                li.setRangeY(0, 10);
            }
            li.setRangeX(0, 10);
            li.setLineToFill(0);

            li.setOnPointClickedListener(new OnPointClickedListener() {

                @Override
                public void onClick(int lineIndex, int pointIndex) {
                    Toast.makeText(getActivity(),
                            "$" + prices.get(pointIndex).getValue() + " " + prices.get(pointIndex).getUnit(),
                            Toast.LENGTH_SHORT)
                            .show();
                }
            });
        }

        return v;
    }



}
