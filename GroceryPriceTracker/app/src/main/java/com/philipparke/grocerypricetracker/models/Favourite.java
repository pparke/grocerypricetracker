package com.philipparke.grocerypricetracker.models;

/**
 * Favourite
 * Defines model for favourite products
 */
public class Favourite {

    private long id;
    private String name;
    private String product_id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductId() {
        return product_id;
    }

    public void setProductId(String product_id) {
        this.product_id = product_id;
    }

    @Override
    public String toString() {
        return name;
    }
}
