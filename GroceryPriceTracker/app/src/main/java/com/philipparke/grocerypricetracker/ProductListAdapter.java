package com.philipparke.grocerypricetracker;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.philipparke.grocerypricetracker.com.androidhive.imagefromurl.ImageLoader;
import com.philipparke.grocerypricetracker.database.FavouritesDataSource;
import com.philipparke.grocerypricetracker.database.FavouritesDbHelper;
import com.philipparke.grocerypricetracker.models.Product;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Product List Adapter
 * Sets up the product list and attaches event handlers.
 */
public class ProductListAdapter extends ArrayAdapter<Product> {
    private final Context context;
    private final List<Product> values;
    private final FavouritesDataSource datasource;


    /**
     * Ctor
     * Set context and values
     * @param context
     * @param values
     */
    public ProductListAdapter(Context context, List<Product> values, FavouritesDataSource datasource) {
        super(context, R.layout.product_list_row, values);
        this.context = context;
        this.values = values;
        this.datasource = datasource;
    }

    /**
     * Get View
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Product product = values.get(position);

        // get the inflater
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the layout
        View rowView = inflater.inflate(R.layout.product_list_row, parent, false);

        // get the text view
        TextView textView = (TextView) rowView.findViewById(R.id.product_list_row_label);

        // get the image view
        ImageView imageView = (ImageView) rowView.findViewById(R.id.product_list_row_image);

        // set the text on the text view
        textView.setText(product.getName());

        // set the image on the image view
        ImageLoader imgLoader = new ImageLoader(context);
        imgLoader.DisplayImage(product.getImage(), R.drawable.notfound2, imageView);



        // Get the button
        final Button button = (Button) rowView.findViewById(R.id.addfavourite);

        final boolean inFavourites = datasource.containsId(product.getId());

        // If the product is already in favourites
        if(inFavourites) {
            button.setText("Remove from Favourites");
        }

        // Set the on click listener for the button
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Added " + product.getName() + " to Favourites", Toast.LENGTH_SHORT).show();
                // Add or remove item from favourites
                if(inFavourites) {
                    datasource.deleteFavourite(product.getId());
                    Toast.makeText(getContext(), "Added " + product.getName() + " to Favourites", Toast.LENGTH_SHORT).show();
                    button.setText("Add to Favourites");
                } else {
                    datasource.createFavourite(product.getName(), product.getId());
                    Toast.makeText(getContext(), "Removed " + product.getName() + " from Favourites", Toast.LENGTH_SHORT).show();
                    button.setText("Remove from Favourites");
                }
            }
        });

        return rowView;
    }



}
